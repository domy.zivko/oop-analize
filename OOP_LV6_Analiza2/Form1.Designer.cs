﻿namespace OOP_LV6_Analiza2 {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.letterPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblLives = new System.Windows.Forms.Label();
            this.triesPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // letterPanel
            // 
            this.letterPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.letterPanel.Location = new System.Drawing.Point(0, 0);
            this.letterPanel.Name = "letterPanel";
            this.letterPanel.Padding = new System.Windows.Forms.Padding(20);
            this.letterPanel.Size = new System.Drawing.Size(1156, 119);
            this.letterPanel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblLives);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 185);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1156, 50);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Preostalo pokušaja: ";
            // 
            // lblLives
            // 
            this.lblLives.AutoSize = true;
            this.lblLives.Location = new System.Drawing.Point(112, 17);
            this.lblLives.Name = "lblLives";
            this.lblLives.Size = new System.Drawing.Size(19, 13);
            this.lblLives.TabIndex = 1;
            this.lblLives.Text = "99";
            // 
            // triesPanel
            // 
            this.triesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.triesPanel.Location = new System.Drawing.Point(0, 119);
            this.triesPanel.Name = "triesPanel";
            this.triesPanel.Padding = new System.Windows.Forms.Padding(20);
            this.triesPanel.Size = new System.Drawing.Size(1156, 66);
            this.triesPanel.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1156, 235);
            this.Controls.Add(this.triesPanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.letterPanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel letterPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblLives;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel triesPanel;
    }
}

