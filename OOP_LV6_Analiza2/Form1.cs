﻿//Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, 
//i u svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu 
//funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala, 
//dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. 


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP_LV6_Analiza2 {

    public partial class Form1 : Form {

        private List<string> terms = new List<string>();
        private List<char> tries = new List<char>();

        private string term;
        private int hits;
        private int lives;

        private string LivesView {
            get { return lives.ToString(); }
        }

        public Form1() {
            InitializeComponent();
        }

        private void NewTerm() {
            if(terms.Count == 0) {
                MessageBox.Show("To je sve");
                Application.Exit();
            }

            tries.Clear();

            triesPanel.Controls.Clear();
            letterPanel.Controls.Clear();

            lives = 10;
            hits = 0;
            lblLives.Text = LivesView;

            int rand = new Random().Next(terms.Count);
            term = terms[rand];

            foreach (char c in term) {
                Label letterLabel = new Label();
                if (c == ' ' || c == ',')
                    letterLabel.Font = new Font(FontFamily.GenericMonospace, 13f);
                else
                    letterLabel.Font = new Font(FontFamily.GenericMonospace, 13f, FontStyle.Underline);
                letterLabel.Text = c == ',' ? "," : " ";
                letterLabel.AutoSize = true;
                letterLabel.Margin = Padding.Empty;
                letterPanel.Controls.Add(letterLabel);
            }
        }

        private void Form1_Load(object sender, EventArgs e) {
            using (System.IO.StreamReader reader = new System.IO.StreamReader("terms.txt")) {
                string line;
                while((line = reader.ReadLine()) != null) {
                    terms.Add(line);
                }
            }

            NewTerm();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e) {
            if (tries.Contains(e.KeyChar)) return;

            tries.Add(e.KeyChar);

            Label letterLabel = new Label();
            letterLabel.Font = new Font(FontFamily.GenericMonospace, 10f);
            letterLabel.AutoSize = true;
            letterLabel.Text = e.KeyChar.ToString();
            triesPanel.Controls.Add(letterLabel);

            bool hit = false;
            for (int i = 0; i < term.Length; i++) {
                char letter = term[i];
                if (letter == e.KeyChar) {
                    if (letterPanel.Controls[i].Text == " ") {
                        hit = true;
                        hits++;
                        letterPanel.Controls[i].Text = letter.ToString();
                    }
                }
            }

            if (hits == term.Length - term.Count(c => c == ' ' || c == ',')) {
                MessageBox.Show("Veri najs");
                terms.Remove(term);
                NewTerm();
            }

            if (!hit) {
                lives--;
                lblLives.Text = LivesView;

                if (lives == 0) {
                    MessageBox.Show("Haha fejl");
                    NewTerm();
                }
            }

        }
    }
}
