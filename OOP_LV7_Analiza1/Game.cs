﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP_LV7_Analiza1 {

    enum PlayerMark {
        NONE, P1, P2
    }

    class Game {

        public string Player1_name { get; private set; }
        public string Player2_name { get; private set; }

        public PlayerMark CurrentPlayer { get; private set; }

        public string CurrentPlayerName {
            get {
                return CurrentPlayer == PlayerMark.P1 ?
                    Player1_name : Player2_name;
            }
        }

        public bool Started {
            get {
                return Player1_name != null && Player1_name.Count() != 0 &&
                    Player2_name != null && Player2_name.Count() != 0;
            }
        }

        private PlayerMark[] boardState;
        public PlayerMark[] BoardState {
            get { return boardState; }
            private set {
                if (boardState == value) return;
                boardState = value;
                OnBoardChanged(new EventArgs());
            }
        }

        public Game() {
            BoardState = new PlayerMark[9];
            Reset();
        }

        public event EventHandler BoardChanged;

        private void OnBoardChanged(EventArgs e) {
            BoardChanged?.Invoke(this, e);
        }

        private void Reset() {
            CurrentPlayer = PlayerMark.P1;

            for (int i = 0; i < 9; i++) {
                BoardState[i] = PlayerMark.NONE;
            }

            OnBoardChanged(new EventArgs());
        }

        public void Start(string player1_name, string player2_name) {
            Player1_name = player1_name;
            Player2_name = player2_name;
            Reset();
        }

        public void PlayMove(int fieldIndex) {
            if (!Started || boardState[fieldIndex] != PlayerMark.NONE) return;

            boardState[fieldIndex] = CurrentPlayer;
            OnBoardChanged(new EventArgs());

            if (CheckWin(fieldIndex)) {
                MessageBox.Show(CurrentPlayerName + " has won!");
                Reset();
            } else {
                CurrentPlayer = CurrentPlayer == PlayerMark.P1 ?
                    PlayerMark.P2 : PlayerMark.P1;
            }

        }

        private bool CheckWin(int playedIndex) {
            int x = playedIndex % 3;
            int y = playedIndex / 3;

            // Check all 8 directions
            for (int dy = -1; dy < 3; dy++) {
                for (int dx = -1; dx < 3; dx++) {
                    if (dx == 0 && dy == 0) continue;

                    if (CheckHit(x + dx, y + dy, CurrentPlayer)) {
                        // If we hit in this direction, check the next step - 
                        // if it is not a hit, check the opposite direction
                        if (CheckHit(x + 2 * dx, y + 2 * dy, CurrentPlayer)) {
                            return true;
                        } else {
                            if (CheckHit(x - dx, y - dy, CurrentPlayer)) {
                                return true;
                            }
                        }
                    }

                }
            }

            return false;
        }

        private bool CheckHit(int x, int y, PlayerMark pm) {
            if (x >= 0 && x < 3 && y >= 0 && y < 3) {
                if (boardState[x + y * 3] == pm) return true;
            }
            return false;
        }

    }

    static class ExtensionMethods {

        public static string GetSymbol(this PlayerMark pm) {
            switch (pm) {
                case PlayerMark.P1:
                    return "X";
                case PlayerMark.P2:
                    return "O";
                default:
                    return "";
            }
        }

        public static Color GetColor(this PlayerMark pm) {
            switch (pm) {
                case PlayerMark.P1:
                    return Color.Red;
                case PlayerMark.P2:
                    return Color.RoyalBlue;
                default:
                    return Color.Black;
            }
        }
    }
}
