﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP_LV7_Analiza1 {

    public partial class Form1 : Form {

        private Game game = new Game();

        private Label[] boardLabels;

        public Form1() {
            InitializeComponent();

            game.BoardChanged += Game_BoardChanged;

            boardLabels = new Label[9];
            for(int i = 0; i < 9; i++) {
                Label lbl = new Label {
                    Font = new Font("Microsoft Sans Serif",
                    36F,
                    System.Drawing.FontStyle.Bold,
                    System.Drawing.GraphicsUnit.Point,
                    ((byte)(238))),
                    Dock = DockStyle.Fill,
                    TextAlign = ContentAlignment.MiddleCenter
                };
                lbl.Click += boardLabel_Click;

                boardLabels[i] = lbl;
                board.Controls.Add(lbl);
            }

            RefreshTurnLabel();
        }

        private void Game_BoardChanged(object sender, EventArgs e) {
            for (int i = 0; i < 9; i++) {
                PlayerMark pm = ((Game)sender).BoardState[i];
                boardLabels[i].Text = pm.GetSymbol();
                boardLabels[i].ForeColor = pm.GetColor();
            }
        }

        private void boardLabel_Click(object sender, EventArgs e) {
            game.PlayMove(Array.IndexOf(boardLabels, sender));
            RefreshTurnLabel();
        }

        private void btnStart_Click(object sender, EventArgs e) {
            if(tbPlayer1.TextLength == 0 || tbPlayer2.TextLength == 0) {
                MessageBox.Show("Please enter player names");
                return;
            }

            game.Start(tbPlayer1.Text, tbPlayer2.Text);
            RefreshTurnLabel();
        }

        private void RefreshTurnLabel() {
            lblTurnPlayer.Text = game.CurrentPlayerName;
            lblTurnPlayer.ForeColor = game.CurrentPlayer.GetColor();
        }
    }
}
