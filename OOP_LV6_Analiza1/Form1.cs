﻿//Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost 
//znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) i barem 
//5 naprednih (sin, cos, log, sqrt...) operacija.


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP_LV6_Analiza1 {

    public partial class Form1 : Form {

        public Form1() {
            InitializeComponent();
        }

        private bool ParseOperands(out double val1, out double val2) {
            if (!double.TryParse(tbOperand1.Text, out val1))
                MessageBox.Show("Operand 1 invalid");
            else if (!double.TryParse(tbOperand2.Text, out val2))
                MessageBox.Show("Operand 2 invalid");
            else return true;

            val1 = val2 = 0;
            return false;
        }

        private bool ParseUnary(out double val1) {
            if (!double.TryParse(tbOperand1.Text, out val1))
                MessageBox.Show("Operand 1 invalid");
            else return true;

            val1 = 0;
            return false;
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            if (ParseOperands(out double val1, out double val2)) {
                lblAns.Text = (val1 + val2).ToString();
            }
        }

        private void btnSubtract_Click(object sender, EventArgs e) {
            if (ParseOperands(out double val1, out double val2)) {
                lblAns.Text = (val1 - val2).ToString();
            }
        }

        private void btnMultiply_Click(object sender, EventArgs e) {
            if (ParseOperands(out double val1, out double val2)) {
                lblAns.Text = (val1 * val2).ToString();
            }
        }

        private void btnDivide_Click(object sender, EventArgs e) {
            if (ParseOperands(out double val1, out double val2)) {
                lblAns.Text = (val1 / val2).ToString();
            }
        }

        private void btnPow_Click(object sender, EventArgs e) {
            if (ParseOperands(out double val1, out double val2)) {
                lblAns.Text = Math.Pow(val1, val2).ToString();
            }
        }

        private void btnRoot_Click(object sender, EventArgs e) {
            if (ParseOperands(out double val1, out double val2)) {
                lblAns.Text = Math.Pow(val1, 1/val2).ToString();
            }
        }

        private void btnSin_Click(object sender, EventArgs e) {
            if(ParseUnary(out double val1)) {
                lblAns.Text = Math.Sin(val1).ToString();
            }
        }

        private void btnCos_Click(object sender, EventArgs e) {
            if (ParseUnary(out double val1)) {
                lblAns.Text = Math.Cos(val1).ToString();
            }
        }

        private void btnLog_Click(object sender, EventArgs e) {
            if (ParseOperands(out double val1, out double val2)) {
                lblAns.Text = Math.Log(val1, val2).ToString();
            }
        }
    }
}
